new Vue({
    el: '#app',
    data(){
        return {
            numPrueba:1,
            numImputs:[],
            valoresGenerados:[],
            ValidacionNumPrueba:{
                valido:true,
                msj:'',
            },
            vizualizarDatos:false,
        }
    },
    computed:{
        validacionPrueba(){
            return this.numPrueba < 1 ? {valido:false, msj:'El numero de prueba debe ser mayor a 1'} : this.numPrueba > 5000 ? {valido:false, msj:'El numero de prueba debe ser menor 5000'} : {valido:true, msj:''}
        }
    },
    methods: {
        generarInputs() {

            if(!this.validacionPrueba.valido)
                return false;

            let num = parseInt(this.numPrueba);
            let newInputs = [];
            for (let i = 0; i < num; i++) {
                let matriz = {dimension: '',matriz:[], nombre:'M'+(i+1)};
                newInputs.push(matriz);
            }
            this.numImputs = newInputs;
        },
        obtenerLecturas() {
            let dimension = this.numImputs;
            let matrizGenerada = [];
            let datosGenerados = [];
            let datosOriginales = [];
            let maxDimencion = Math.pow(10, 9);

            dimension.forEach((m) => {
                let data = m;
                let dimen = m.dimension;
                dimen = dimen.split(",");
                let fila = parseInt(dimen[0]);
                let columna = parseInt(dimen[1]);
                let dimencionMatriz = fila * columna;

                if(!isNaN(fila) && !isNaN(columna)){
                    if(dimencionMatriz <= maxDimencion){
                        //data.matriz = this.generarMatriz(fila, columna);
                        data.matriz = [fila, columna];
                        data = _.omit(data,['error']);
                        matrizGenerada.push(data);
                    }
                    else {
                        data.error = true;
                        data.msj = 'La dimencion solicitada sobre pasa (10^9)';
                        matrizGenerada.push(data);
                    }

                } else{
                    data.error = true;
                    data.msj = 'Valor vacio o caracter invalido';
                    matrizGenerada.push(data);
                }
            });

            matrizGenerada.forEach((p) => {
                if(!p.error){
                    let info = this.procesarLectura(p.matriz);
                    // p.recorrido = info.posicionesR;
                    // p.direcciones = info.direccionesR;
                    p.direccionFinal = info.direccionF;
                }
                datosGenerados.push(p);
            });

            this.numImputs = datosGenerados;
            this.valoresGenerados = _.reject(datosGenerados,(p)=>{ return p.error});
            if(!_.isEmpty(this.valoresGenerados)) {
                this.vizualizarDatos = true;
            }
            else {
                this.vizualizarDatos = false;
            }

        },
        generarMatriz(filas, columnas, valorPre) {
            var matrizDatos = [];

            for (var i = 0; i < filas; i++) {
                matrizDatos.push([]);
                matrizDatos[i].push(new Array(columnas));

                for (var j = 0; j < columnas; j++) {
                    matrizDatos[i][j] = valorPre;
                }
            }
            return matrizDatos;
        },

        procesarLectura(matriz){
            let fila = 0;
            let columna = 0;
            let recorrido = 'R';
            let paso = 0;
            let totalElementos = (matriz[0] * matriz[1]);
            let infoMatriz = {posicionesR:[], direccionesR:[]};

            while (paso < totalElementos) {
                let data = {};
                //infoMatriz.direccionesR.push(recorrido);
                infoMatriz.direccionF =recorrido;
                if(recorrido==='R'){
                    data = this.leerDerecha(matriz, fila, columna);
                    paso += data.recorridos;
                }

                if(recorrido==='D'){
                    data = this.leerAbajo(matriz, fila, columna);
                    paso += data.recorridos;
                }

                if(recorrido==='L'){
                    data = this.leerIzquierda(matriz, fila, columna);
                    paso += data.recorridos;
                }

                if(recorrido==='U'){
                    data = this.leerArriba(matriz, fila, columna);
                    paso += data.recorridos;
                    fila+=1;
                    columna+=1;
                }
                //infoMatriz.posicionesR.push(...data.posiciones);
                recorrido = data.direccion;
            }
            return infoMatriz;
        },
        leerDerecha (matriz, fila, columna, callback){
            let data = {recorridos:0, direccion:'D', posiciones:[]};
            let inicio = fila;
            let final =  matriz[1] - columna;
            for(let i = inicio ; i < final; i++){
                //let posicion = columna+''+i;
                //data.posiciones.push(posicion);
                data.recorridos +=1;
            };

            return data;
        },
        leerAbajo(matriz, fila, columna){
            let data = {recorridos:0, direccion:'L', posiciones:[]};
            let inicio = fila + 1;
            let final = matriz[0] - fila;
            let columnaOriginal = matriz[1] - 1 - columna;
            for(let i = inicio; i < final; i++){
                data.recorridos +=1;
                //let posicion = i+''+columnaOriginal;
                //data.posiciones.push(posicion);
            }
            return data;
        },
        leerIzquierda(matriz, fila, columna){
            let data = {recorridos:0, direccion:'U', posiciones:[]};
            let inicio = matriz[1] - 2 - columna;
            let final = columna;
            let filaOriginal = matriz[0] - 1 - fila;
            for(let i = inicio; i >= final; i--){
                data.recorridos +=1;
                // let posicion = filaOriginal+''+i;
                ///data.posiciones.push(posicion);
            }
            return data;
        },
        leerArriba(matriz, fila, columna){
            let data = {recorridos:0, direccion:'R', posiciones:[]};
            let inicio = matriz[0] - 2 - fila;
            let final = fila + 1;
            for(let i = inicio ; i >= final; i--){
                // let posicion = i+''+columna;
                //data.posiciones.push(posicion);
                data.recorridos +=1;
            }
            return data;
        }
    }
});