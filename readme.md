
## Requerimientos

- Vue.js v2.5.17
- Bootstrap v4.1.3

## Instalacion

- clonar el repositorio y verificar los requerimientos

## Ejecutar

- Abrir el archivo index.html en el navegador
- El primer campo solicita el numero de pruebas a realizar
- Los campos desplegados solicitan el tamaño de la matriz, dos numeros separados con ','
- Si los datos ingresados son correctos desplegara una tabla con los recorridos y el recorrido final

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
